pragma solidity ^0.4.2;

import "../installed_contracts/oraclize-api/contracts/usingOraclize.sol";
import "../contracts/CoinHouseOfBets.sol";
import "../contracts/SafeMath.sol";

contract SmartBetMatch is usingOraclize, Owned {
    using SafeMath for uint;
    address public owner;
    CoinHouseOfBets public hob;

    string public urlJsonApiEvent;
    uint256 public minimumBet;
    Matche public matche;
    bool public openBet;

    uint256 home_team_totalCoins;
    uint256 away_team_totalCoins;

    enum Team { not_team, home_team, away_team }
    enum typeGame { winner, loser}

    struct InGamePlayer {
        uint coins;        
        Team selectionWinner;
        bool reclaim;
    }

    enum MethodOraclize { finish, winner }
    struct oraclizeSend {
        bool status;
        MethodOraclize methodOraclize;
    }

    struct Matche {
        uint64 name;
        uint64 home_team;
        uint64 away_team;
        Team winner;
        uint date;
        bool finished;
    }

    mapping(address => InGamePlayer) public InGamePlayers;
    mapping(bytes32 => oraclizeSend) validIds;

    event newOraclizeQuery(string description);
    event updateEvent(string description);
    event InPlayerBetEvent(string description);
    event ReclaimPlayerBetEvent(string description, uint256 tokenWineer);
    event calResultEvent(uint inCoin, uint totalWinner, uint percentage, uint percentageDis, uint away_totalCoins, uint home_totalCoins);
    
    constructor (CoinHouseOfBets token, address ora) public {
        require(token != address(0));

        if(ora != address(0)){
            OAR = OraclizeAddrResolverI(ora);
        }
        home_team_totalCoins = 0;
        away_team_totalCoins = 0;
        hob = token;
        owner = msg.sender;    
        openBet = false;    
    }
   
    // ------------------------------------------------------------------------
    // Don't accept ETH
    // ------------------------------------------------------------------------
    function () public payable {
        revert();
    }

    function kill() public {
        require(msg.sender != owner);
        selfdestruct(owner);
    }

    function __callback(bytes32 myid, string result) public  {
        // TODO: Add mesage error
        oraclizeSend memory oraSend = validIds[myid];
        require(oraSend.status);
        require(msg.sender == oraclize_cbAddress()); 
        if(oraSend.methodOraclize == MethodOraclize.finish) {
            updateFinish(result);    
        }
        else if(oraSend.methodOraclize == MethodOraclize.winner) {
            updateWinner(result);   
        }
        else {
            emit updateEvent("Not winner or finish. ");  
        }
        delete oraSend; // Remove in storage?          
    }

    function IntanceEventBet(string urlApiEventDeclare, uint64 name, uint64 home, uint64 away, uint date) public onlyOwner {
        require(!openBet); 
        require((bytes(urlApiEventDeclare)).length != 0, "urlApiEventDeclare Required parameters");
        require(name != 0x0, "name Required parameters");
        require(home != 0x0, "home Required parameters");
        require(away != 0x0, "away Required parameters");
        require(date != 0x0, "date Required parameters");
        
        urlJsonApiEvent = urlApiEventDeclare;
        Matche memory matcheArr = matche; 
        matcheArr.name = name;
        matcheArr.home_team = home;
        matcheArr.away_team = away;
        matcheArr.date = date;
        matcheArr.finished = false;
        matche = matcheArr;
        openBet = true;
        Update();
    }

    function updateFinish(string result) private {
        // TODO: create Safe (math) and (string)
        Matche storage matcheArr = matche;
        if(keccak256(result) == keccak256("true")) {
            matcheArr.finished = true; 
        } 
        else {
            string memory mesage = strConcat("not FINISH Match. result: ", result);  
            emit updateEvent(mesage);
        }     
        matche = matcheArr;
    }

    function updateWinner(string result) private {
        Matche storage matcheArr = matche;
        if(keccak256(result) == keccak256("home")) {
            matcheArr.winner = Team.home_team; //"home"; 
            emit updateEvent("FINISH and Winner is: home");
        }
        else if(keccak256(result) == keccak256("away")) {
            matcheArr.winner = Team.away_team; //"away"; 
            emit updateEvent("FINISH and Winner is: away");
        } 
        else {
            string memory mesage = strConcat("not FINISH Match. result: ", result);  
            emit updateEvent(mesage);
        }  
        matche = matcheArr;       
    }

    function Update() payable public {
        string memory urlApi;
        if (oraclize_getPrice("URL") > address(this).balance) {
            emit newOraclizeQuery("Oraclize query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            bytes32 queryId;

            if(!matche.finished){
                urlApi = strConcat(urlJsonApiEvent, ".finished");
                queryId = oraclize_query("URL", urlApi);
                validIds[queryId].methodOraclize = MethodOraclize.finish;
                validIds[queryId].status = true;
                emit newOraclizeQuery("Oraclize query was sent finish, standing by for the answer..");
            } else  {     
                urlApi = strConcat(urlJsonApiEvent, ".winner");
                queryId = oraclize_query("URL", urlApi);
                validIds[queryId].methodOraclize = MethodOraclize.winner;
                validIds[queryId].status = true;
                emit newOraclizeQuery("Oraclize query was sent winner, standing by for the answer..");
            }
        }
    }

    function InPlayerBet(uint team, uint256 moneyToken) public {
        require(openBet); // Open Bet
        require(uint(Team.away_team) >= team);
        require(moneyToken > 0);
        require(!matche.finished); // denegate new player post finish game
        
        InGamePlayer memory player = InGamePlayers[msg.sender];
        player.coins = player.coins.add(moneyToken);
        player.selectionWinner = Team(team);
        player.reclaim = false;

        if(player.selectionWinner == Team.home_team) {
            home_team_totalCoins = home_team_totalCoins.add(moneyToken);
        } else if(player.selectionWinner == Team.away_team) {
            away_team_totalCoins = away_team_totalCoins.add(moneyToken);
        }
        else {
            require(Team(team) == Team.not_team);
        } 

        bool result = hob.transferFrom(msg.sender, address(this), moneyToken);

        if(result) {            
            InGamePlayers[msg.sender] = player;
            emit InPlayerBetEvent("Player add in Game. ");
        }
        else {
            emit InPlayerBetEvent("CoinHouseOfBets return false in transfer. ");
        }
    }

    function ReclaimPlayerBet() public {
        require(matche.finished, "you already claim what won. ");

        InGamePlayer memory player = InGamePlayers[msg.sender];
        require(!player.reclaim);

        if(player.selectionWinner == matche.winner){

            // Calculate result
            uint winner = 0;
            uint totalWinner = 0;
            
            uint percentage = 0;
            uint percentageDis = 0;

            if(player.selectionWinner == Team.away_team){
                // participation
                percentage = (player.coins.mul(100)) / (away_team_totalCoins);

                // disadvantaged
                percentageDis = (home_team_totalCoins.mul(100)) / (100);
                winner = percentageDis.mul(percentage);
            }
            else if (player.selectionWinner == Team.home_team) {
                // participation
                percentage = (player.coins.mul(100)) / (home_team_totalCoins);

                // disadvantaged
                percentageDis = (away_team_totalCoins.mul(100)) / (100);
                winner = percentageDis.mul(percentage);                
            }

            totalWinner = player.coins + (winner / 10**2);
            player.reclaim = true;
            emit calResultEvent(player.coins, totalWinner, percentage, percentageDis, away_team_totalCoins, home_team_totalCoins);
            hob.transfer(msg.sender, totalWinner);
            emit ReclaimPlayerBetEvent("Your game has been finalized and your money has been sent. ", totalWinner);
        }
        else {
            string memory mesage;
            if(player.selectionWinner == Team.away_team){
                mesage = "You selected a home away";
            }
            else if (player.selectionWinner == Team.home_team) {
                mesage = "You selected a home Team";
            }
            else {
                mesage = "You selected a default and not finished. "; 
            }

            if(matche.winner == Team.away_team){
                mesage = strConcat(mesage, " and winner is away. ");
            }
            else if (matche.winner == Team.home_team) {
                mesage = strConcat(mesage, " and winner is home. ");
            }
            else {
                mesage = strConcat(mesage, " and we do not have a winner. ");
            }

            emit ReclaimPlayerBetEvent(mesage, 0);
        }
        
    }

}
