pragma solidity ^0.4.2;

import "../contracts/SafeMath.sol";
import "../contracts/Owner.sol";
import "../contracts/CoinHouseOfBets.sol";
import "../contracts/SmartBetMatch.sol";

contract SmartHouseOfBets is Owned {
    CoinHouseOfBets public hob;
    
    address private owner;
    
    mapping(address => SmartBetMatch) public smartBetMatchList;
    
    event NotificateSmartBetMatch(address from, address _contract);

    constructor(CoinHouseOfBets token) public {

        hob = token;
        owner = msg.sender;
    }
    
    // ------------------------------------------------------------------------
    // Don't accept ETH
    // ------------------------------------------------------------------------
    function () public payable {
        revert();
    }

    function AddSmartBetMatch(address smartBetMatch) public {
        require (msg.sender == owner);    
        smartBetMatchList[smartBetMatch]; 
        emit NotificateSmartBetMatch(msg.sender, smartBetMatch);
    } 
    
/* obsolet
    function NewBetMatch(string urlApiEventDeclare, uint64 name, uint64 home, uint64 away, uint date) public {
        require (msg.sender == owner);    
        address smartBetMatch = new SmartBetMatch(hob, urlApiEventDeclare, name, home, away, date);   
        smartBetMatchList[smartBetMatch]; 
        emit NotificateSmartBetMatch(msg.sender, smartBetMatch);
    } */
}
