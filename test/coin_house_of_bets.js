const CoinHouseOfBets = artifacts.require('CoinHouseOfBets')

function hobToWei(){
  return 1024 * Math.pow(10, 18);
}

contract('CoinHouseOfBets', function(accounts) {
  
  it("Deploy and Validate Balance", function() {
    return  CoinHouseOfBets.deployed().then(function(coinHouseOfBets) {
      return coinHouseOfBets.balanceOf.call(accounts[0]);
    }).then(function(result){      
      assert.equal(result.valueOf(), 128000000000000000000, 'accounts[0] balance is wrong');
    })     
  })  

  it("Total Supply", function() {
    return  CoinHouseOfBets.deployed().then(function(coinHouseOfBets) {
      return coinHouseOfBets.totalSupply.call();
    }).then(function(result){      
      assert.equal(result.valueOf(), 128000000000000000000, 'accounts[0] balance is wrong');
    })     
  })  

  it("Buy Tokens", function() {
    var coinHouseOfBets;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[1], value: web3.toWei('1', 'ether')})
    }).then(function(result){
      return coinHouseOfBets.balanceOf.call(accounts[1]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[1] should have 1024 HOB");
    })    
  })  

  it("Transfer Tokens", function() {
    var coinHouseOfBets;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return coinHouseOfBets.transfer(accounts[2], 512, { from: accounts[1] });
    }).then(function(result){  
      return coinHouseOfBets.balanceOf.call(accounts[2]);
    }).then(function(result){      
      assert.equal(result.valueOf(), 512, 'accounts[2] balance is wrong');
    })     
  })  

  it("Transfer From Tokens", function() {
    var coinHouseOfBets;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return coinHouseOfBets.approve(accounts[2], 256, { from: accounts[1] });
    }).then(function(result){  
      assert.equal(result.logs[0].args.tokenOwner, accounts[1], 'approve tokenOwner event correct');
      assert.equal(result.logs[0].args.spender, accounts[2], 'approve spender event correct');
      assert.equal(result.logs[0].args.tokens, 256, 'approve Return tokens correct');

      return coinHouseOfBets.transferFrom(accounts[1], accounts[3], 256, { from: accounts[2] });
    }).then(function(result){    
      return coinHouseOfBets.balanceOf.call(accounts[3]);  

    }).then(function(result){    
      assert.equal(result.valueOf(), 256, 'accounts[2] balance is wrong');
    })       
  })  

});
