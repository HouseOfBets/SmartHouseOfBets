# SmartWorldCup

Contrato Inteligente para apuestas de la copa mundial de fútbol 2018.
Esto es un desarrollo experimental. Pero tu puedes usar mi experiencia :)
 

**Desarrolladores**  

* Nicolas A. Catalogna <nicocatalogna@gmail.com> (Desarrollo y Concepto)

**Agradecimientos**

* Juan Carlos Quintero <juanquinterodj@gmail.com> (Ayuda de concepto)
* Alguien que siendo un liberal, siempre ayuda con las ideas.


**Documentation**
* [Logica Reparto](./LogicaReparto.md)
* [Logica Apuesta](./LogicaApuesta.md)
* [Herramientas](./Herramientas.md)

**TODO**

Automatizar test de Oracle

Desde 'ethereum-bridge\config\instance\oracle_instance_*.js' se puede obtener la direccion ORA e implementar el contrato en el test. 


**Pre Requisitos**

*Install*
 * [NodeJs](https://nodejs.org/es/)
 * [Python](https://www.python.org/downloads/release/python-2714/)
 * [Ganache](http://truffleframework.com/ganache/)

**Tools**
* [ethereum-bridge](https://github.com/oraclize/ethereum-bridge) Local deployment oracle
* [ganache-cli](https://github.com/trufflesuite/ganache-cli)  *not required


*Commandos*
 * npm install -g truffle
 * truffle install oraclize

*Run*
 * truffle migrate
 * truffle test 

**En caso de Error**

*migrate*
* truffle migrate --reset

*test* 
Error Oracle

Implementar en la maquina local, ethereum-bridge y correrlo con el comando 'node bridge -H localhost:8545 -a 1'. 

TODO: Al contrato [SmartBetMach.sol - L52](https://gitlab.com/ncatalogna/SmartWorldCup/blob/master/contracts/SmartBetMatch.sol#L52) se permite configurar. modificar el migrator con la direccion obtenido por **ethereum-bridge** y remplazarlo en [2_houseofbets_migration.js - L9](https://gitlab.com/ncatalogna/SmartWorldCup/blob/master/migrations/2_houseofbets_migration.js#L9).

Esto no es dinamico para salida productiva, se buscara una mejor opcion.


En caso de querer omitor esto. Correr solo.

* truffle  test ./test/coin_house_of_bets.js


**Consola: Log test**
```
  Contract: SmartHouseOfBets
    √ Deploy SmartHouseOfBets and SmartBetMatch, test AddSmartBetMatch (85ms)
    √ SmartBetMatch test IntanceEventBet (653ms)
    √ Buy Tokens Player 1 and in game (1050ms)
    √ Buy Tokens Player 2 and in game (837ms)
    √ Buy Tokens Player 3 and in game (738ms)
    √ Buy Tokens Player 4 and in game (739ms)
    √ Buy Tokens Player 5 and in game (890ms)
    √ Buy Tokens Player 6 and in game (805ms)
    √ SmartBetMatch test Update post IntanceEventBet (15372ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 1 and loser (10251ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 4 and winner (10331ms)
    √ SmartBetMatch test ReclaimPlayerBet 4 player claims for the second time  (10040ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 5 and winner (10185ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 6 and winner (10202ms)

  Contract: CoinHouseOfBets
    √ Deploy and Validate Balance (86ms)
    √ Total Supply
    √ Buy Tokens (525ms)
    √ Transfer Tokens (246ms)
    √ Transfer From Tokens (240ms)


  19 passing (1m)
```

