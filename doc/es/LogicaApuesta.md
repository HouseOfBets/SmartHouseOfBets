# Lógica de Apuesta


**Esquema Flujo de Vida**

```mermaid
graph TD
A[Oraculo]
C[Listado Partido]
D{Encuentra Partido}
E{Estado Partido}
K{Existe Contrato}
G(Call Update)
F(Finaliza Ciclo)
T[Informacion Expuesta]
N(Crear Contrato)
M(Setea Datos)
subgraph API 
 C --> D
 D -- No --> F
 D -- Si --> K
 E -- En Curso --> F
 F -- Actualiza --> T
 K -- Si --> E
 end 
 subgraph SmartBetMatch 
 E -- Finalizado --> G 
 K -- No --> N 
 N -- Agrega --> M 
 end
G -- Consulta --> A
A -- Escucha --> T
```

```mermaid
graph TD
J(Participante)
CO(Token HOB)
 subgraph SmartBetMatch 
 A{Ingreso habilitado}
 N(Intente mas tarde)
 O{Oraculo Actualizo}
 I(Apuesta)
 L{Finalizo Partido}
 R(Reclamar Dividendo)
 OR[Oraculo no responde]
 A -- Si --> I
 A -- No --> N
 I -- Consultan --> O
 O -- Si --> L
 O -- Sin resultado --> OR 
 L -- No --> I
 L -- Si --> R
 end
 subgraph CoinHouseOfBets
 J -- Compra --> CO
 CO -- Apuesta --> A
 end
```