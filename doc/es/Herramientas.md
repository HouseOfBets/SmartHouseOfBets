#Herramientas

**watch event**

Los siguientes Script, permiten obtener informacion de eventos declarados y generados en la instancia.

```solidity
    event IntanceEventBetEvent(uint64 name, uint64 home, uint64 away, uint date, bool finished, string moment);
```

```javascript
    let intanceEventBetEvent = smartBetMatch.IntanceEventBetEvent({_from: accounts[0]}, {fromBlock: 0, toBlock: 'latest'});

    intanceEventBetEvent.watch(function(error, result){
        if (error != null)
            console.log(error);

        console.log(result);
    });
```

siendo **IntanceEventBetEvent** un evento emitido.

y el retorno es:

```json
{ logIndex: 2,
  transactionIndex: 0,
  transactionHash: '0xfb85bd5e65e4f5baef9deb602b7d4d9a5485de05599abe9b7885b9f66d31d07d',
  blockHash: '0xb30413e87f2bcb54447d4e0f35badc23a1ff4de0be9fb5651eb2da108b71e2a7',
  blockNumber: 679,
  address: '0x18db094b3312ed25fccb3444ac7401eb867c9b2e',
  type: 'mined',
  event: 'IntanceEventBetEvent',
  args:
   { name: { [String: '64'] s: 1, e: 1, c: [Object] },
     home: { [String: '61'] s: 1, e: 1, c: [Object] },
     away: { [String: '62'] s: 1, e: 1, c: [Object] },
     date: { [String: '1531634400'] s: 1, e: 9, c: [Object] },
     finished: true,
     moment: 'Update' } }
```

**Delay en Mocha**

En caso que se requiera un delay en la prueba [make-mocha-wait-before-running-next-test](https://stackoverflow.com/questions/25537808/make-mocha-wait-before-running-next-test)

