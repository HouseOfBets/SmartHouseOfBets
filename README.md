# SmartWorldCup
Soccer World Cup Russia 2018 Intelligent bet Contract. This is a development experiment. But you can take the experience :)

**Development members**

* Nicolas A. Catalogna <nicocatalogna@gmail.com>

**Special Thanks**

* Juan Carlos Quintero <juanquinterodj@gmail.com> (Help in the concept)
* Someone trained as a liberal always helps with ideas.

**Documentation**

* [README-Es](./doc/es/README.md)
  
**TODO**

Oracle Test Automation

From the 'ethereum-bridge\config\instance\oracle_instance_*.js' file obtain and set the Ora in the deploy of the contract involved

**Pre Requisitos**

*Install*
 * [NodeJs](https://nodejs.org/es/)
 * [Python](https://www.python.org/downloads/release/python-2714/)
 * [Ganache](http://truffleframework.com/ganache/)

**Tools**
* [ethereum-bridge](https://github.com/oraclize/ethereum-bridge) Local deployment oracle
* [ganache-cli](https://github.com/trufflesuite/ganache-cli)  *not required


*Commandos*
 * npm install -g truffle
 * truffle install oraclize

*Run*
 * truffle migrate
 * truffle test 

**In case of error**

*migrate*
truffle migrate --reset

*test* 
*Error Oracle*

deploy on local machine ethereum-bridge and run. example commandos 'node bridge -H localhost:8545 -a 1'

TODO: A cleaner way of configuring ORA is required

it is allowed to configure. modify the migrator with the address obtained by **ethereum-bridge** and replace it in [2_houseofbets_migration.js - L9](https://gitlab.com/ncatalogna/SmartWorldCup/blob/master/migrations/2_houseofbets_migration.js#L9).

This is not dynamic for productive output, a better option is sought.

Otherwise omit and run
* truffle  test ./test/smart_house_of_bets.js

**Console: Log test**
```
  Contract: SmartHouseOfBets
    √ Deploy SmartHouseOfBets and SmartBetMatch, test AddSmartBetMatch (85ms)
    √ SmartBetMatch test IntanceEventBet (653ms)
    √ Buy Tokens Player 1 and in game (1050ms)
    √ Buy Tokens Player 2 and in game (837ms)
    √ Buy Tokens Player 3 and in game (738ms)
    √ Buy Tokens Player 4 and in game (739ms)
    √ Buy Tokens Player 5 and in game (890ms)
    √ Buy Tokens Player 6 and in game (805ms)
    √ SmartBetMatch test Update post IntanceEventBet (15372ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 1 and loser (10251ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 4 and winner (10331ms)
    √ SmartBetMatch test ReclaimPlayerBet 4 player claims for the second time  (10040ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 5 and winner (10185ms)
    √ SmartBetMatch test ReclaimPlayerBet Player 6 and winner (10202ms)

  Contract: CoinHouseOfBets
    √ Deploy and Validate Balance (86ms)
    √ Total Supply
    √ Buy Tokens (525ms)
    √ Transfer Tokens (246ms)
    √ Transfer From Tokens (240ms)


  19 passing (1m)
```

